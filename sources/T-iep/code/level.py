from groups import AllSprites
from player import Player
from settings import *
from sprites import Sprite, MovingSprite, AnimatedSprite

class Level:
	def __init__(self, tmx_map, level_frames, jump_sfx, data, switch_stage):
		self.display_surface = pygame.display.get_surface()
		self.data = data
		self.switch_stage = switch_stage

		# level data 
		self.level_width = tmx_map.width * TILE_SIZE
		self.level_bottom = tmx_map.height * TILE_SIZE
		tmx_level_properties = tmx_map.get_layer_by_name('Data')[0].properties
		self.level_unlock = tmx_level_properties['level_unlock']
		bg_tile = None

		# groups 
		self.all_sprites = AllSprites(
			width = tmx_map.width, 
			height = tmx_map.height,
			bg_tile = bg_tile, 
			top_limit = tmx_level_properties['top_limit'], 
			horizon_line = tmx_level_properties['horizon_line'])
		self.collision_sprites = pygame.sprite.Group()
		self.semi_collision_sprites = pygame.sprite.Group()
		self.item_sprites = pygame.sprite.Group()

		self.setup(tmx_map, level_frames, jump_sfx)


	def setup(self, tmx_map, level_frames, jump_sfx):
		# tiles 
		for layer in ['BG', 'Terrain', 'Platforms']:
			for x, y, surf in tmx_map.get_layer_by_name(layer).tiles():
				groups = [self.all_sprites]
				if layer == 'Terrain': groups.append(self.collision_sprites)
				if layer == 'Platforms': groups.append(self.semi_collision_sprites)

				if layer == 'BG': z = Z_LAYERS['bg tiles']
				else: z = Z_LAYERS['main']

				Sprite((x * TILE_SIZE,y * TILE_SIZE), surf, groups, z)

		
		# objects 
		for obj in tmx_map.get_layer_by_name('Objects'):
			if obj.name == 'player':
				self.player = Player(
					pos = (obj.x, obj.y), 
					groups = self.all_sprites, 
					collision_sprites = self.collision_sprites, 
					semi_collision_sprites = self.semi_collision_sprites,
					frames = level_frames['player'], 
					data = self.data, 
					jump_sound = jump_sfx)
			else:
					# frames 
					frames = level_frames[obj.name]

					# groups 
					groups = [self.all_sprites]

					# z index
					z = Z_LAYERS['main']

					# animation speed
					animation_speed = ANIMATION_SPEED
					AnimatedSprite((obj.x, obj.y), frames, groups, z, animation_speed)
			if obj.name == 'end':
				self.level_finish_rect = pygame.FRect((obj.x, obj.y), (obj.width, obj.height))

		# moving objects 
		for obj in tmx_map.get_layer_by_name('Moving Objects'):
			frames = level_frames[obj.name]
			groups = (self.all_sprites, self.semi_collision_sprites) if obj.properties['platform'] else (self.all_sprites, self.damage_sprites)
			if obj.width > obj.height: # horizontal
				move_dir = 'x'
				start_pos = (obj.x, obj.y + obj.height / 2)
				end_pos = (obj.x + obj.width,obj.y + obj.height / 2)
			else: # vertical 
				move_dir = 'y'
				start_pos = (obj.x + obj.width / 2, obj.y)
				end_pos = (obj.x + obj.width / 2,obj.y + obj.height)
			speed = obj.properties['speed']
			MovingSprite(frames, groups, start_pos, end_pos, move_dir, speed, obj.properties['flip'])
   

	def check_constraint(self):
		# left right
		if self.player.hitbox_rect.left <= 0:
			self.player.hitbox_rect.left = 0
		if self.player.hitbox_rect.right >= self.level_width:
			self.player.hitbox_rect.right = self.level_width

		# bottom border 
		if self.player.hitbox_rect.bottom > self.level_bottom:
			self.switch_stage('overworld', -1)

		# success 
		if self.player.hitbox_rect.colliderect(self.level_finish_rect):
			self.switch_stage('overworld', self.level_unlock)

	def run(self, dt):
		self.display_surface.fill('black')
		
		self.all_sprites.update(dt)
		self.check_constraint()
		
		self.all_sprites.draw(self.player.hitbox_rect.center, dt)