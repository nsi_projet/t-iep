from pytmx.util_pygame import load_pygame
from os.path import join

from data import Data
from level import Level
from overworld import Overworld
from settings import * 
from support import * 

class Game:
	def __init__(self):
		pygame.init()
		self.display_surface = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
		pygame.display.set_caption('T-iep')
		self.clock = pygame.time.Clock()
		self.import_assets()

		self.data = Data()
		self.tmx_maps = {
			0: load_pygame(join('data', 'levels', '0.tmx')),
			1: load_pygame(join('data', 'levels', '1.tmx')),
			2: load_pygame(join('data', 'levels', '2.tmx')),
			3: load_pygame(join('data', 'levels', '3.tmx')),
			4: load_pygame(join('data', 'levels', '4.tmx')),
			5: load_pygame(join('data', 'levels', '5.tmx')),
			}
		self.tmx_overworld = load_pygame(join('data', 'overworld', 'overworld.tmx'))
		self.current_stage = Level(self.tmx_maps[self.data.current_level], self.level_frames, self.jump_sfx, self.data, self.switch_stage)
		self.bg_music.play(-1)

	def switch_stage(self, target, unlock = 0):
		if target == 'level':
			self.current_stage = Level(self.tmx_maps[self.data.current_level], self.level_frames, self.jump_sfx, self.data, self.switch_stage)
			
		else: # overworld 
			if unlock > 0:
				self.data.unlocked_level = 6
			self.current_stage = Overworld(self.tmx_overworld, self.data, self.overworld_frames, self.switch_stage)

	def import_assets(self):										
		self.level_frames = {
			'end': import_folder('graphics', 'level', 'end'),
			'player': import_sub_folders('graphics','player'),
			'moving_platform': import_folder('graphics', 'level', 'moving_platform'),
		}

		self.overworld_frames = {
			'path': import_folder_dict('graphics', 'overworld', 'path'),
			'icon': import_image('graphics', 'overworld', 'icon'),
		}

		self.jump_sfx = pygame.mixer.Sound(join('audio', 'jump.wav'))
		self.jump_sfx.set_volume(0.25)
		self.bg_music = pygame.mixer.Sound(join('audio', 'deepdive.mp3'))
		self.bg_music.set_volume(0.4)

	def run(self):
		self.current_stage = Overworld(self.tmx_overworld, self.data, self.overworld_frames, self.switch_stage)
		while True:
			dt = self.clock.tick() / 1000
			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					pygame.quit()
					sys.exit()

			self.current_stage.run(dt)
			
			pygame.display.update()

if __name__ == '__main__':
	game = Game()
	game.run()