<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.9" tiledversion="1.9.1" name="objects" tilewidth="96" tileheight="96" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="29">
  <image width="96" height="96" source="../../graphics/objects/player.png"/>
 </tile>
 <tile id="30">
  <image width="64" height="64" source="../../graphics/objects/end.png"/>
 </tile>
</tileset>
